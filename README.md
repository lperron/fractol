# Fract'Ol

<p align='center'>Frac'Ol is a fractals generating software using the minilibx. It's the second graphical project of 42 school.<\p>
<p align='center'>This project can generate fractals using two techniques : an escape-time algorithm (Julia set, Mandelbrot set, etc.) and a L-systems interpreter using strings rewriting and turtle graphics to render fractals.<\p>

## Compilation & Usage

```
~ git clone https://gitlab.com/lperron/fractol.git
~ cd fractol/
~ make
```
Then to use the escape time algorithm :

```
~ ./fractol (FRACTAL)
```
You can choose various fractals :
- Julia
- Mandelbrot
- Burning (Burning Ship)
- Tricorn
- OogieBoogie
- Orchis
- JuliaB (a mix of Julia Set and Burning Ship)

Or to use the L-systems interpreter :

```
~ ./fractol -l FILE.ls
```

Where FILE.ls is the path to a valid L-system file, there's several examples in the ex/ directory (or you can write your own!).

## Screenshots

 <figure>
  <img src="https://gitlab.com/lperron/fractol/raw/master/Screenshots/fractolex4.png" alt="Classic Julia Set" style="width:100%">
  <figcaption>Classic Julia Set</figcaption>
</figure>

 <figure>
  <img src="https://gitlab.com/lperron/fractol/raw/master/Screenshots/fractolex1.png" alt="Detail of Julia's Burning at the power 4" style="width:100%">
  <figcaption>Detail of Julia's Burning at the power 4</figcaption>
</figure> 

 <figure>
  <img src="https://gitlab.com/lperron/fractol/raw/master/Screenshots/fractolex2.png" alt="Detail of Julia's Burning at the power 4" style="width:100%">
  <figcaption>Detail of Julia's Burning at the power 4</figcaption>
</figure> 

 <figure>
  <img src="https://gitlab.com/lperron/fractol/raw/master/Screenshots/fractolex3.png" alt="Detail of Julia at the power 1.9" style="width:100%">
  <figcaption>Detail of Julia at the power 1.9</figcaption>
</figure> 

 <figure>
  <img src="https://gitlab.com/lperron/fractol/raw/master/Screenshots/fractolex5.png" alt="L-system Plant" style="width:100%">
  <figcaption>L-system Plant</figcaption>
</figure> 

# Video

<figure>
  <img src="https://gitlab.com/lperron/files_for_other_projects/raw/master/DemoFractGit.mp4" alt="Psychedelism" style="width:100%">
</figure> 
