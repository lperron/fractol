#Hexa Koch
AXIOM :
A-A+A-A+A-A

RULES :
F:F-F++F-F
A:F++F++F



PARAMS :
angle:60
start_angle:75
length:5
scale_factor:2
startx:0.5
starty:0.5
iter:5
