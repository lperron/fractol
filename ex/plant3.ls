#Plant 3
AXIOM :
VZFFF

RULES :
V:[+++W][---W]YV
W:+X[-W]Z
X:-W[+X]Z
Y:YZ
Z:[-FFF][+FFF]F

PARAMS :
angle:20
start_angle:-90
length:12
startx:0.5
starty:0.2
iter:11
