#Sierpinski Square
AXIOM :
F+XF+F+XF

RULES :
X:XF-F+F-XF+F+XF-F+F-X
PARAMS :
angle: 90
start_angle:-90
length:16
startx:0.25
starty:0.4
iter:4
