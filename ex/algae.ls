#Algae
AXIOM :
aF

RULES :
a:FFFFFv[+++h][---q]fb
b:FFFFFv[+++h][---q]fc
c:FFFFFv[+++fa]fd
d:FFFFFv[+++h][---q]fe
e:FFFFFv[+++h][---q]fg
g:FFFFFv[---fa]fa
h:ifFF
i:fFFF[--m]j
j:fFFF[--n]k
k:fFFF[--o]l
l:fFFF[--p]
m:fFn
n:fFo
o:fFp
p:fF
q:rfF
r:fFFF[++m]s
s:fFFF[++n]t
t:fFFF[++o]u
u:fFFF[++p]
v:Fv
PARAMS :
angle:12
start_angle:-90
length:3
startx:0.5
starty:0.
iter:20
