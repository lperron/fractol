#Hexagonal Gosper
AXIOM :
XF

RULES :
X:X+YF++YF-FX--FXFX-YF+
Y:-FX+YFYF++YF+FX--FX-Y

PARAMS :
angle:60
start_angle:-90
length:7
startx:0.3
starty:0.75
iter:5
