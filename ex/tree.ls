#Tree
AXIOM :
FX

RULES :
X:>[-FX]+[FX]

PARAMS :
angle:40
start_angle:-90
length:470
startx:0.5
starty:0.0
scale_factor:0.6
iter:12
