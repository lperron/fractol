#Sierpinski Arrowhead

AXIOM :
F

RULES :
F:G-F-G
G:F+G+F

PARAMS :
iter:7
angle:60
starty:0.9
start_angle:120
length:7
