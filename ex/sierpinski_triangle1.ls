#Sierpinski Triangle

AXIOM :
F-G-G

RULES :
F:F-G+F+G-F
G:GG

PARAMS :
angle:120
length:13
iter:6
starty:0.9
start_angle:120
