#Bushes
AXIOM :
a

RULES :
F:>F<
a:F[+x]Fb
b:F[-y]Fa
x:a
y:b

PARAMS :
angle:45
start_angle:-90
length:2
startx:0.5
starty:0.23
scale_factor:1.36
iter:13
