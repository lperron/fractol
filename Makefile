# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2019/05/09 12:30:59 by lperron      #+#   ##    ##    #+#        #
#    Updated: 2019/08/15 17:07:28 by lperron     ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

CC= gcc
SRCDIR= ./srcs/
FILES= complex1.c complex2.c complex3.c main.c julia.c t_point.c mandelbrot.c utils.c thread.c mouse_hook.c print_info1.c print_info2.c set_fractal.c key_hook.c mlx_things.c lsys_input.c lsys_recur.c lsys_interpreter.c bresenham.c lsys_key_hook.c lsys_print_info.c lsys_check.c lsys.c lsys_main.c palette.c palette_switch.c
SRC= $(addprefix $(SRCDIR),$(FILES))
OBJ= $(SRC:.c=.o)
INC= ./includes/complex.h ./includes/fractol.h ./includes/t_point.h ./includes/lsys.h
INCFLAG= -I ./libft/includes/ -I ./minilibx/ -I ./includes/
all: libs fractol

libs:
	make -C ./libft/
	cp ./libft/libft.a ./libft.a
ifeq ($(shell uname -s),Linux)
	make -C ./minilibx/
	cp ./minilibx/libmlx_Linux.a ./libmlx.a
endif
ifeq ($(shell uname -s),Darwin)
	make -C ./minilibx_macos/
	cp ./minilibx_macos/libmlx.a ./libmlx.a
endif

%.o: %.c $(INC)
	$(CC) -c -o $@ $< $(INCFLAG) -Werror -Wextra -Wall -pthread -O3 -march=native
	
fractol: $(OBJ) $(INC)
ifeq ($(shell uname -s),Linux)
	$(CC) -o fractol $(OBJ)  -L . -lft -lm -L/usr/X11/lib -lX11 -lmlx -lXext -Wall -Wextra -pthread
endif
ifeq ($(shell uname -s),Darwin)
	$(CC) -o fractol $(OBJ) -L . -lft -lm -framework OpenGl -framework Appkit -lmlx -Wall -Wextra -Werror
endif

clean: 
	make -C ./libft/ clean
	make -C ./minilibx/ clean
	make -C ./minilibx_macos/ clean
	rm -f ./srcs/*.o

fclean: clean
	make -C ./libft/ fclean
	make -C ./minilibx/ clean
	make -C ./minilibx_macos/ clean
	rm -f ./fractol


re: fclean all
