/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mandelbrot.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/11 21:13:59 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 16:37:21 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"
#include <math.h>

void		mandel_point(t_point p, t_param *param, t_mlx_struct *ml)
{
	double		i;
	t_complex	z;
	t_complex	c;
	double		tmp;
	int			radius;

	i = -1;
	c = mapset(p, param);
	z = init_complex(0, 0);
	radius = param->radius * param->radius;
	while (++i < param->max_iter && z.a * z.a + z.b * z.b < radius)
		z = cadd(cpowerfast(z, param->power.x, param->power.y), c);
	if (i < param->max_iter && param->cmode != NORM)
	{
		tmp = log(log((z.a * z.a + z.b * z.b) / 2.)) / log(2);
		i = i + 2 - tmp;
	}
	plot_point(p, i, param, ml);
}

void		tricorn_point(t_point p, t_param *param, t_mlx_struct *ml)
{
	double		i;
	t_complex	z;
	t_complex	c;
	double		tmp;
	int			radius;

	i = -1;
	c = mapset(p, param);
	z = init_complex(0, 0);
	radius = param->radius * param->radius;
	while (++i < param->max_iter && z.a * z.a + z.b * z.b < radius)
		z = cadd(cpowerfast(cconj(z), param->power.x,
			param->power.y), c);
	if (i < param->max_iter && param->cmode != NORM)
	{
		tmp = log(log((z.a * z.a + z.b * z.b) / 2.)) / log(2);
		i = i + 2 - tmp;
	}
	plot_point(p, i, param, ml);
}

void		burning_point(t_point p, t_param *param, t_mlx_struct *ml)
{
	double		i;
	t_complex	z;
	t_complex	c;
	double		tmp;
	int			radius;

	i = -1;
	c = mapset(p, param);
	z = init_complex(0, 0);
	radius = param->radius * param->radius;
	while (++i < param->max_iter && z.a * z.a + z.b * z.b < radius)
	{
		z.a = fabs(z.a);
		z.b = fabs(z.b);
		z = cadd(cpowerfast(cconj(z),
			param->power.x, param->power.y), c);
	}
	if (i < param->max_iter && param->cmode != NORM)
	{
		tmp = log(log((z.a * z.a + z.b * z.b) / 2.)) / log(2);
		i = i + 2 - tmp;
	}
	plot_point(p, i, param, ml);
}

void		lemp1_point(t_point p, t_param *param, t_mlx_struct *ml)
{
	double		i;
	t_complex	z;
	t_complex	c;
	double		tmp;
	int			radius;

	i = -1;
	c = mapset(p, param);
	z = init_complex(0, 0);
	radius = param->radius * param->radius;
	while (++i < param->max_iter && z.a * z.a + z.b * z.b < radius)
	{
		z.a = fabs(z.a);
		z.b = (z.b);
		z = cadd(cpowerfast(cconj(z),
			param->power.x, param->power.y), c);
	}
	if (i < param->max_iter && param->cmode != NORM)
	{
		tmp = log(log((z.a * z.a + z.b * z.b) / 2.)) / log(2);
		i = i + 2 - tmp;
	}
	plot_point(p, i, param, ml);
}

void		lemp2_point(t_point p, t_param *param, t_mlx_struct *ml)
{
	double		i;
	t_complex	z;
	t_complex	c;
	double		tmp;
	int			radius;

	i = -1;
	c = mapset(p, param);
	z = init_complex(0, 0);
	radius = param->radius * param->radius;
	while (++i < param->max_iter && z.a * z.a + z.b * z.b < radius)
	{
		z.a = fabs(z.a);
		z.b = -1 * (z.b);
		z = cadd(cpowerfast(cconj(z),
			param->power.x, param->power.y), c);
	}
	if (i < param->max_iter && param->cmode != NORM)
	{
		tmp = log(log((z.a * z.a + z.b * z.b) / 2.)) / log(2);
		i = i + 2 - tmp;
	}
	plot_point(p, i, param, ml);
}
