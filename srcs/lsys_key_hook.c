/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lsys_key_hook.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/13 22:46:40 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 23:31:33 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lsys.h"

int		lsys_destroy_hook(t_lstruct *data)
{
	mlx_destroy_image(data->ml->mlx_ptr, data->ml->img);
	mlx_destroy_window(data->ml->mlx_ptr, data->ml->win_ptr);
	free_sys(data->sys);
	exit(0);
	return (0);
}

int		zoom(int key, t_lsys *sys)
{
	double		mod;
	double		old_length;
	t_complex	new_strt;

	mod = key == I_CHAR ? 1.22 : 0.833333333;
	old_length = sys->length;
	sys->length *= mod;
	if (sys->length < 1)
	{
		sys->length = old_length;
		return (0);
	}
	new_strt = sys->strt_point;
	new_strt.a -= 0.5 * SCREEN_X;
	new_strt.b -= 0.5 * SCREEN_Y;
	new_strt.a *= mod;
	new_strt.b *= mod;
	new_strt.a += 0.5 * SCREEN_X;
	new_strt.b += 0.5 * SCREEN_Y;
	sys->strt_point = new_strt;
	return (1);
}

int		changeiter(int key, t_lsys *sys)
{
	if (key == MINUS_CHAR)
	{
		if (sys->iter == 0)
			return (0);
		else
			sys->iter--;
		if (!(lsys_recur(sys)) &&
				ft_printf("Error : Bad Sentence\n"))
			exit(0);
		return (1);
	}
	sys->iter++;
	if (get_inser_size(sys->str_iter, sys) > 10000000)
	{
		sys->iter--;
		return (0);
	}
	if (!(lsys_recur(sys)) && ft_printf("Error : Bad Sentence\n"))
		exit(0);
	return (1);
}

int		lsys_handle_key_pressed(int key, t_lstruct *data)
{
	if (key == UP_CHAR)
		data->arrow[0] = 1;
	if (key == DOWN_CHAR)
		data->arrow[1] = 1;
	if (key == LEFT_CHAR)
		data->arrow[2] = 1;
	if (key == RIGHT_CHAR)
		data->arrow[3] = 1;
	if (key == I_CHAR || key == O_CHAR)
		data->sys->change = zoom(key, data->sys);
	if (key == PLUS_CHAR || key == MINUS_CHAR)
		data->sys->change = changeiter(key, data->sys);
	data->sys->length = data->sys->length < 1 ? 1 : data->sys->length;
	if (key == ESC_CHAR)
		lsys_destroy_hook(data);
	return (0);
}

int		lsys_handle_key_released(int key, t_lstruct *data)
{
	if (key == UP_CHAR)
		data->arrow[0] = 0;
	if (key == DOWN_CHAR)
		data->arrow[1] = 0;
	if (key == LEFT_CHAR)
		data->arrow[2] = 0;
	if (key == RIGHT_CHAR)
		data->arrow[3] = 0;
	return (0);
}
