/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   main.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/14 01:34:15 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 17:44:46 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"
#include "lsys.h"
#include <time.h>

int		print_usage(void)
{
	ft_putendl("usage :\n./fractol (FRACTAL)\n           Julia");
	ft_putendl("           Mandelbrot\n           Tricorn");
	ft_putendl("           Burning\n           OogieBoogie");
	ft_putendl("           Orchis\n           JuliaB");
	ft_putendl("\n./fractol -l LS_FILE");
	return (0);
}

void	hook_me_up(t_hook *data)
{
	mlx_hook(data->ml->win_ptr, MOTIONNOTIFY, POINTERMOTIONMASK,
			mouse_hook, data);
	mlx_mouse_hook(data->ml->win_ptr, mouse_button_hook, data);
	mlx_hook(data->ml->win_ptr, KEYPRESS, KEYPRESSMASK,
			handle_key_pressed, data);
	mlx_hook(data->ml->win_ptr, KEYRELEASE, KEYRELEASEMASK,
			handle_key_released, data);
	mlx_loop_hook(data->ml->mlx_ptr, loop_hook, data);
	mlx_hook(data->ml->win_ptr, DESTROYNOTIFY,
		STRUCTURENOTIFYMASK, destroy_hook, data);
	mlx_loop(data->ml->mlx_ptr);
}

void	classic_fractol(char f)
{
	t_mlx_struct	ml;
	t_fractalizer	fract;
	t_param			param;
	t_hook			data;
	pthread_t		thr[NB_THREAD];

	srand(time(NULL));
	init_mlx(SCREEN_X, SCREEN_Y, "fractol", &ml);
	param.power = init_point(2, 0);
	param.c = init_complex(-0.75, 0.11);
	param.max_iter = 100;
	param.zoom = 1;
	init_palette(&param);
	param.cmode = SMOOTH;
	param.radius = 4;
	param.beg_space = init_complex(0, 0);
	fract.param = &param;
	fract.change = 1;
	set_fractal(&fract, f);
	data.thr = thr;
	data.fract = &fract;
	data.ml = &ml;
	data.control = 0;
	data.shift = 0;
	hook_me_up(&data);
}

void	classic_fractol_switch(char *str)
{
	if (ft_strcmp("Julia", str) == 0)
		classic_fractol(JULIA);
	else if (ft_strcmp("Mandelbrot", str) == 0)
		classic_fractol(MANDELBROT);
	else if (ft_strcmp("Tricorn", str) == 0)
		classic_fractol(TRICORN);
	else if (ft_strcmp("Burning", str) == 0)
		classic_fractol(BURNING);
	else if (ft_strcmp("OogieBoogie", str) == 0)
		classic_fractol(LEMP1);
	else if (ft_strcmp("Orchis", str) == 0)
		classic_fractol(LEMP2);
	else if (ft_strcmp("JuliaB", str) == 0)
		classic_fractol(JULIAB);
	else
	{
		ft_printf("Error : %s is not a valid fractal\n\n", str);
		print_usage();
	}
}

int		main(int argc, char **argv)
{
	if (argc > 4)
		return (print_usage());
	else if (argc == 1)
		classic_fractol_switch("Julia");
	else if (argc == 2)
		classic_fractol_switch(argv[1]);
	else if (argc == 3 && ft_strcmp(argv[1], "-l") == 0)
		lsys_fractol(argv);
	else
		print_usage();
	return (0);
}
