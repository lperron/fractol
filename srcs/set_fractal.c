/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   set_fractal.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/12 21:54:15 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/12 21:54:40 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"

void		set_fractal(t_fractalizer *fract, int type)
{
	fract->type = type;
	if (type == JULIA)
		fract->fract_func = julia_point;
	if (type == MANDELBROT)
		fract->fract_func = mandel_point;
	if (type == TRICORN)
		fract->fract_func = tricorn_point;
	if (type == BURNING)
		fract->fract_func = burning_point;
	if (type == LEMP1)
		fract->fract_func = lemp1_point;
	if (type == LEMP2)
		fract->fract_func = lemp2_point;
	if (type == JULIAB)
		fract->fract_func = juliab_point;
}

void		next_fractal(t_fractalizer *fract)
{
	if (fract->type == JULIA)
		set_fractal(fract, MANDELBROT);
	else if (fract->type == MANDELBROT)
		set_fractal(fract, TRICORN);
	else if (fract->type == TRICORN)
		set_fractal(fract, BURNING);
	else if (fract->type == BURNING)
		set_fractal(fract, LEMP1);
	else if (fract->type == LEMP1)
		set_fractal(fract, LEMP2);
	else if (fract->type == LEMP2)
		set_fractal(fract, JULIAB);
	else if (fract->type == JULIAB)
		set_fractal(fract, JULIA);
}
