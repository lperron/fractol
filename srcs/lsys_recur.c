/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lsys_recur.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/13 12:06:37 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 19:28:31 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lsys.h"
#include "libft.h"
#include "t_point.h"

int	get_inser_size(char *str, t_lsys *sys)
{
	int	size;
	int	len_rep;
	int	i;
	int	r;

	r = -1;
	size = ft_strlen(str);
	while (++r < sys->num)
	{
		len_rep = ft_strlen(&sys->rules[r][2]) - 1;
		i = -1;
		while (str[++i])
			if (str[i] == sys->rules[r][0])
				size += len_rep;
	}
	return (size);
}

int	apply_rules(t_lsys *sys)
{
	t_point		w;
	int			k;
	int			r;
	char		*tmp;
	int			sub;

	if (!(tmp = ft_strnew(get_inser_size(sys->str_iter, sys))))
	{
		ft_memdel((void**)sys->str_iter);
		return (0);
	}
	w = init_point(-1, -1);
	while (sys->str_iter[++w.x] && (r = -1) &&
			!(sub = 0))
	{
		while (++r < sys->num && (k = 1))
			if (sys->str_iter[w.x] == sys->rules[r][0] && (sub = 1))
				while (sys->rules[r][++k])
					tmp[++w.y] = sys->rules[r][k];
		if (!sub)
			tmp[++w.y] = sys->str_iter[w.x];
	}
	free(sys->str_iter);
	sys->str_iter = tmp;
	return (1);
}

int	check_brackets(char *str)
{
	int	bra;
	int	ket;
	int	i;

	i = -1;
	bra = 0;
	ket = 0;
	while (str[++i])
	{
		if (str[i] == '[')
			bra++;
		if (str[i] == ']')
			ket++;
		if (ket > bra)
			return (0);
	}
	return (bra == ket);
}

int	lsys_recur(t_lsys *sys)
{
	int	i;

	ft_memdel((void**)&sys->str_iter);
	if (!(sys->str_iter = ft_strdup(sys->axiom)))
		return (0);
	i = -1;
	while (++i < sys->iter)
	{
		if (!(apply_rules(sys)))
			return (0);
	}
	if (!check_brackets(sys->str_iter))
	{
		ft_memdel((void**)&sys->str_iter);
		return (0);
	}
	sys->str_size = ft_strlen(sys->str_iter);
	return (1);
}
