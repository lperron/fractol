/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   bresenham.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/04/25 15:39:13 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/13 23:27:03 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lsys.h"
#include <math.h>

void		plot_dot(t_point coord,
				t_mlx_struct *ml, int col)
{
	int			pos;

	if (coord.x < 0 || coord.y < 0 || coord.x >= SCREEN_X ||
			coord.y >= SCREEN_Y)
		return ;
	pos = coord.x + coord.y * SCREEN_X;
	ml->img_data[pos] = mlx_get_color_value(ml->mlx_ptr, col);
}

void		bresenham_low(t_point *v, t_point *w, t_mlx_struct *ml)
{
	t_point	dxy;
	int		yi;
	t_point c;
	int		d;

	dxy = init_point(w->x - v->x, 2 * (w->y - v->y));
	yi = 1;
	if (dxy.y < 0 && (yi = -1))
		dxy.y *= -1;
	d = dxy.y - dxy.x;
	dxy.x *= 2;
	c = init_point(v->x - 1, v->y);
	while (++c.x < w->x)
	{
		plot_dot(c, ml, lempx_rgb(255, 255, 255));
		if (d > 0)
		{
			c.y += yi;
			d -= dxy.x;
		}
		d += dxy.y;
	}
}

void		bresenham_hi(t_point *v, t_point *w, t_mlx_struct *ml)
{
	t_point	dxy;
	int		xi;
	t_point c;
	int		d;

	dxy = init_point(2 * (w->x - v->x), w->y - v->y);
	xi = 1;
	if (dxy.x < 0 && (xi = -1))
		dxy.x *= -1;
	d = dxy.x - dxy.y;
	dxy.y *= 2;
	c = init_point(v->x, v->y - 1);
	while (++c.y < w->y)
	{
		plot_dot(c, ml, lempx_rgb(255, 255, 255));
		if (d > 0)
		{
			c.x += xi;
			d -= dxy.y;
		}
		d += dxy.x;
	}
}

void		draw_line(t_point *v1, t_point *v2, t_mlx_struct *ml)
{
	if ((v1->x > SCREEN_X && v2->x > SCREEN_X) ||
		(v1->x < 0 && v2->x < 0) ||
		(v1->y > SCREEN_Y && v2->y > SCREEN_Y) ||
			(v1->y < 0 && v2->y < 0))
		return ;
	if (ft_abs(v2->y - v1->y) <
			ft_abs(v2->x - v1->x))
	{
		if (v1->x > v2->x)
			bresenham_low(v2, v1, ml);
		else
			bresenham_low(v1, v2, ml);
	}
	else
	{
		if (v1->y > v2->y)
			bresenham_hi(v2, v1, ml);
		else
			bresenham_hi(v1, v2, ml);
	}
}
