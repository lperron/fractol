/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   thread.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/12 21:23:17 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 19:35:15 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"
#include <pthread.h>

int		fract_thread(void *dt)
{
	t_thread_data *data;

	data = (t_thread_data*)dt;
	while (data->strt.y < SCREEN_Y)
	{
		while (data->strt.x < SCREEN_X)
		{
			data->fract->fract_func(data->strt, data->fract->param, data->ml);
			data->strt.x += NB_THREAD;
		}
		data->strt.y++;
		data->strt.x -= SCREEN_X;
	}
	return (0);
}

void	fractalize(t_fractalizer *fract, t_mlx_struct *ml, pthread_t *thr)
{
	int				i;
	t_thread_data	thr_data[NB_THREAD];

	i = 0;
	while (i < NB_THREAD)
	{
		thr_data[i].strt = init_point(i, 0);
		thr_data[i].fract = fract;
		thr_data[i].ml = ml;
		pthread_create(&(thr[i]), NULL, (void*)fract_thread, &(thr_data[i]));
		i++;
	}
	i = 0;
	while (i < NB_THREAD)
	{
		pthread_join(thr[i], NULL);
		i++;
	}
}
