/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   palette_switch.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/15 17:04:58 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 17:21:23 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"

void	init_palette5(t_param *p)
{
	p->i_pal = 4;
	p->pal_size = 20;
	p->palette[0].color = 4636184;
	p->palette[1].color = 9573735;
	p->palette[2].color = 5668924;
	p->palette[3].color = 13433889;
	p->palette[4].color = 3909853;
	p->palette[5].color = 6029401;
	p->palette[6].color = 10913690;
	p->palette[7].color = 7494118;
	p->palette[8].color = 10826570;
	p->palette[9].color = 15016254;
	p->palette[10].color = 15088107;
	p->palette[11].color = 10510303;
	p->palette[12].color = 593601;
	p->palette[13].color = 11799865;
	p->palette[14].color = 10060520;
	p->palette[15].color = 14598516;
	p->palette[16].color = 9347965;
	p->palette[17].color = 8972831;
	p->palette[18].color = 16388866;
	p->palette[19].color = 3661813;
}

void	init_paletter(t_param *p)
{
	int s;
	int	i;

	i = -1;
	s = 1 + rand() % 250;
	p->pal_size = s;
	while (++i < s)
	{
		p->palette[i].color = 0;
		p->palette[i].rgb[0] = rand() % 256;
		p->palette[i].rgb[1] = rand() % 256;
		p->palette[i].rgb[2] = rand() % 256;
	}
}

void	switch_pal(t_hook *data)
{
	if (data->shift)
		init_paletter(data->fract->param);
	else if (data->fract->param->i_pal == 0)
		init_palette2(data->fract->param);
	else if (data->fract->param->i_pal == 1)
		init_palette3(data->fract->param);
	else if (data->fract->param->i_pal == 2)
		init_palette4(data->fract->param);
	else if (data->fract->param->i_pal == 3)
		init_palette5(data->fract->param);
	else if (data->fract->param->i_pal == 4)
		init_palette(data->fract->param);
}
