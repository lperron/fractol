/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mlx_things.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/12 22:02:15 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 18:33:31 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"
#include <unistd.h>

void		print_frame(t_hook *data)
{
	int		i;
	int		j;
	int		k;
	t_color	tmp;
	int		pixel;

	j = -1;
	while (++j < 95 - (data->fract->type != JULIA && data->fract->type
		!= JULIAB ? 15 : 0) && (i = -1))
	{
		while (++i < 175 + X_MORE + (data->fract->type == JULIAB ?
					JULIA_OFF : 0)
				&& (k = -1))
		{
			pixel = i + j * SCREEN_X;
			tmp.color = data->ml->img_data[pixel];
			while (++k < 4)
				tmp.rgb[k] = tmp.rgb[k] > 100 ?
					tmp.rgb[k] - 100 : 0;
			data->ml->img_data[pixel] = tmp.color;
		}
	}
}

int			loop_hook(t_hook *data)
{
	if (data->fract->change)
	{
		fractalize(data->fract, data->ml, data->thr);
		print_frame(data);
		mlx_put_image_to_window(data->ml->mlx_ptr,
			data->ml->win_ptr, data->ml->img, 0, 0);
		data->fract->change = 0;
		print_info(data);
	}
	else
		usleep(1000);
	return (0);
}

int			init_img(t_mlx_struct *tt)
{
	if (!(tt->img_data = (int*)mlx_get_data_addr(tt->img,
	&(tt->bits_per_pixel), &(tt->size_line), &(tt->endian))))
	{
		mlx_destroy_image(tt->mlx_ptr, tt->img);
		mlx_destroy_window(tt->mlx_ptr, tt->win_ptr);
		return (0);
	}
	return (1);
}

int			init_mlx(int x, int y, char *name, t_mlx_struct *tt)
{
	tt->mlx_ptr = NULL;
	tt->win_ptr = NULL;
	tt->img = NULL;
	if (!(tt->mlx_ptr = mlx_init()) ||
	!(tt->win_ptr = mlx_new_window(tt->mlx_ptr, x, y, name)) ||
	!(tt->img = mlx_new_image(tt->mlx_ptr, x, y)))
	{
		if (tt->win_ptr)
			mlx_destroy_window(tt->mlx_ptr, tt->win_ptr);
		return (0);
	}
	tt->nb_pxl = x * y;
	if (init_img(tt) == 0)
		return (0);
	return (1);
}
