/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   polar_complex.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/10 17:45:32 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 14:33:51 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "complex.h"
#include <math.h>

double		cmod(t_complex c)
{
	return (sqrt(c.a * c.a + c.b * c.b));
}

double		cargu(t_complex c)
{
	return (atan2(c.b, c.a));
}

t_complex	c_from_polar(double r, double th)
{
	return (init_complex(r * cos(th), r * sin(th)));
}

t_complex	cpowerfast(t_complex c, int ni, int nd)
{
	double	r;
	double	th;

	if (nd == 0)
		return (cpowint(c, ni));
	r = cmod(c);
	th = cargu(c);
	return (c_from_polar(pow(r, ni + nd * 0.1),
		(ni + nd * 0.1) * th));
}
