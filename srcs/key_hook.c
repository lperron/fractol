/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   key_hook.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/12 21:57:16 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 17:40:45 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"

int			destroy_hook(t_hook *data)
{
	mlx_destroy_image(data->ml->mlx_ptr, data->ml->img);
	mlx_destroy_window(data->ml->mlx_ptr, data->ml->win_ptr);
	exit(0);
	return (0);
}

void		handle_key_pressed2(int key, t_hook *data)
{
	if (key == C_CHAR && (data->fract->change = 1))
	{
		data->fract->param->cmode++;
		if (data->fract->param->cmode > 2)
			data->fract->param->cmode = 0;
	}
	else if (key == CTRL_CHAR)
		data->control = 1;
	else if (key == SHIFT_CHAR)
		data->shift = 1;
	else if (key == ESC_CHAR)
		destroy_hook(data);
	data->fract->param->max_iter =
int_range(data->fract->param->max_iter, 1, 10000);
	data->fract->param->radius =
int_range(data->fract->param->radius, 1, 10000);
}

int			change_power(t_param *p, int ud, char shift)
{
	int		mod;
	t_point	old;

	old = init_point(p->power.x, p->power.y);
	mod = ud == UP_CHAR ? 1 : -1;
	if (shift)
		p->power.y += mod;
	else
		p->power.x += mod;
	if (p->power.y == 10)
	{
		p->power.x++;
		p->power.y = 0;
	}
	else if (p->power.y == -1)
	{
		p->power.x--;
		p->power.y = 9;
	}
	if ((p->power.x < 1 && p->power.y < 1) || p->power.x < 0)
		p->power = init_point(0, 1);
	if (p->power.x > 99)
		p->power = init_point(100, 0);
	return (p->power.x != old.x || p->power.y != old.y);
}

int			handle_key_pressed(int key, t_hook *data)
{
	if ((key == UP_CHAR || key == DOWN_CHAR))
		data->fract->change =
		change_power(data->fract->param, key, data->shift);
	else if (key == RIGHT_CHAR && (data->fract->change = 1))
		data->fract->param->max_iter += data->shift ? 1 : 10;
	else if (key == LEFT_CHAR && (data->fract->change = 1))
		data->fract->param->max_iter -= data->shift ? 1 : 10;
	else if (key == R_CHAR && (data->fract->change = 1))
		data->fract->param->radius += data->shift ? 1 : 10;
	else if (key == E_CHAR && (data->fract->change = 1))
		data->fract->param->radius -= data->shift ? 1 : 10;
	else if (key == F_CHAR && (data->fract->change = 1))
		next_fractal(data->fract);
	else if (key == P_CHAR && (data->fract->change = 1))
		switch_pal(data);
	else if (key == SPACE_CHAR && (data->fract->change = 1))
	{
		data->fract->param->zoom = 1;
		data->fract->param->beg_space = init_complex(0, 0);
	}
	handle_key_pressed2(key, data);
	return (0);
}

int			handle_key_released(int key, t_hook *data)
{
	if (key == CTRL_CHAR)
		data->control = 0;
	if (key == SHIFT_CHAR)
		data->shift = 0;
	return (0);
}
