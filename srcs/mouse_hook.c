/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   mouse_hook.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/12 21:38:52 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 19:33:08 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"

int		mouse_button_hook(int button, int x, int y, t_hook *data)
{
	double		dx;
	double		dy;
	t_complex	prevdir;
	double		zoom_factor;

	zoom_factor = data->shift ? ZOOM_FACTOR2 : ZOOM_FACTOR1;
	dx = (x / (double)(SCREEN_X - 1));
	dy = (y / (double)(SCREEN_Y - 1));
	prevdir = init_complex(dx / data->fract->param->zoom, dy /
			data->fract->param->zoom);
	if (button == SCROLL_DOWN && (data->fract->change = 1))
	{
		data->fract->param->zoom *= zoom_factor;
		data->fract->param->beg_space.a += prevdir.a * (1 - 1 / zoom_factor);
		data->fract->param->beg_space.b += prevdir.b * (1 - 1 / zoom_factor);
	}
	if (button == SCROLL_UP && (data->fract->change = 1))
	{
		;
		data->fract->param->zoom /= zoom_factor;
		data->fract->param->beg_space.a += prevdir.a * (1 - zoom_factor);
		data->fract->param->beg_space.b += prevdir.b * (1 - zoom_factor);
	}
	return (0);
}

int		mouse_hook(int x, int y, t_hook *data)
{
	if ((data->fract->type != JULIA && data->fract->type != JULIAB)
			|| !data->control)
		return (0);
	data->fract->param->c = init_complex((x / ((double)SCREEN_X - 1)) * 2 - 1,
			(y / ((double)SCREEN_Y - 1)) * 2 - 1);
	data->fract->change = 1;
	return (0);
}
