/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lsys_check.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/14 18:47:59 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 19:18:20 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lsys.h"

int	check_axiom(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
		if (!ft_isalpha(str[i]) && !(str[i] == '+' || str[i] == '-' ||
				str[i] == '|' || str[i] == '[' ||
				str[i] == ']' || str[i] == '>' || str[i] == '<'))
			return (0);
	return (1);
}

int	check_rule(char *str)
{
	if (ft_strlen(str) < 3)
		return (0);
	if (!ft_isalpha(str[0]))
		return (0);
	if (str[1] != ':')
		return (0);
	return (check_axiom(&str[2]));
}

int	other_check(t_lsys *sys)
{
	int	i;
	int	j;

	i = 0;
	if (!sys->name)
		if (!(sys->name = ft_strdup("Unnamed")))
			return (0);
	while (++i < sys->num && (j = -1))
		while (++j < i)
			if (sys->rules[i][0] == sys->rules[j][0])
				return (0);
	return (1);
}
