/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lsys_print_info.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/14 00:16:04 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 14:34:55 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lsys.h"

void		lsys_print_frame(t_lstruct *data, int lenx)
{
	int		i;
	int		j;
	int		k;
	t_color	tmp;
	int		pixel;

	j = -1;
	while (++j < 50 + Y_MORE && (i = -1))
	{
		while (++i < lenx && (k = -1))
		{
			pixel = i + j * SCREEN_X;
			tmp.color = data->ml->img_data[pixel];
			if (tmp.rgb[2] > 50)
				while (++k < 4)
					tmp.rgb[k] = tmp.rgb[2] > 180 ?
					tmp.rgb[k] - 180 : 0;
			data->ml->img_data[pixel] = tmp.color;
		}
	}
}

void		print_size_str(t_lstruct *data)
{
	char	*str;
	char	tmp[21];
	char	e;
	int		size;

	ft_bzero(tmp, 21);
	ft_memcpy(tmp, "Size Sentence : ", 16);
	e = 0;
	size = data->sys->str_size;
	if (size > 1000000 && (e = 'M'))
		size = size / 1000000.;
	if (size > 1000 && (e = 'K'))
		size = size / 1000.;
	if (!(str = ft_itoa(size)))
		return ;
	ft_memcpy(&tmp[16], str, ft_strlen(str));
	tmp[ft_strlen(tmp)] = e;
	mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
		10, 42, lempx_rgb(255, 255, 255), tmp);
	free(str);
}

void		lsys_print_info(t_lstruct *data)
{
	char	*str;
	char	*iter;

	str = ft_strjoin("L-system : ", data->sys->name);
	if (!str)
		return ;
	mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
		10, 10, lempx_rgb(255, 255, 255), str);
	free(str);
	if (!(iter = ft_itoa(data->sys->iter)))
		return ;
	if (!(str = ft_strjoin("Iterations : ", iter)))
	{
		free(iter);
		return ;
	}
	mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
		10, 26, lempx_rgb(255, 255, 255), str);
	free(iter);
	free(str);
	print_size_str(data);
}
