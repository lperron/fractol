/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   palette.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/14 19:56:10 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 17:08:07 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"

void	init_palette(t_param *p)
{
	p->i_pal = 0;
	p->pal_size = 15;
	p->palette[0].color = 13311;
	p->palette[1].color = 26367;
	p->palette[2].color = 39423;
	p->palette[3].color = 52479;
	p->palette[4].color = 65535;
	p->palette[6].color = 3407871;
	p->palette[7].color = 6750207;
	p->palette[8].color = 10092543;
	p->palette[9].color = 13434879;
	p->palette[11].color = 13421772;
	p->palette[12].color = 13408665;
	p->palette[13].color = 13395558;
	p->palette[14].color = 13382451;
}

void	init_palette2(t_param *p)
{
	p->i_pal = 1;
	p->pal_size = 9;
	p->palette[0].color = 1862058;
	p->palette[1].color = 13311945;
	p->palette[2].color = 14882478;
	p->palette[3].color = 6403315;
	p->palette[4].color = 16451192;
	p->palette[5].color = 5092992;
	p->palette[6].color = 16634867;
	p->palette[7].color = 161299;
	p->palette[8].color = 12565116;
}

void	init_palette3(t_param *p)
{
	p->i_pal = 2;
	p->pal_size = 2;
	p->palette[0].color = 5237028;
	p->palette[1].color = 3367528;
}

void	init_palette4(t_param *p)
{
	p->i_pal = 3;
	p->pal_size = 9;
	p->palette[0].color = 9466058;
	p->palette[1].color = 9025036;
	p->palette[2].color = 9687537;
	p->palette[3].color = 8001482;
	p->palette[4].color = 16181266;
	p->palette[5].color = 6967815;
	p->palette[6].color = 2414210;
	p->palette[7].color = 3940917;
	p->palette[8].color = 8012759;
}
