/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   julia.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/10 21:03:17 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 16:35:38 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"
#include <math.h>

void		julia_point(t_point p, t_param *jul, t_mlx_struct *ml)
{
	double		i;
	t_complex	z;
	double		tmp;
	int			radius;

	i = -1;
	z = mapset(p, jul);
	radius = jul->radius * jul->radius;
	while (++i < jul->max_iter && z.a * z.a + z.b * z.b < radius)
		z = cadd(cpowerfast(z, jul->power.x, jul->power.y), jul->c);
	if (i < jul->max_iter && jul->cmode != NORM)
	{
		tmp = log(log((z.a * z.a + z.b * z.b) / 2.)) / log(2);
		i = i + 2 - tmp;
	}
	plot_point(p, i, jul, ml);
}

void		juliab_point(t_point p, t_param *jul, t_mlx_struct *ml)
{
	double		i;
	t_complex	z;
	double		tmp;
	int			radius;

	i = -1;
	z = mapset(p, jul);
	radius = jul->radius * jul->radius;
	while (++i < jul->max_iter && z.a * z.a + z.b * z.b < radius)
	{
		z.a = fabs(z.a);
		z.b = fabs(z.b);
		z = cadd(cpowerfast(z, jul->power.x, jul->power.y), jul->c);
	}
	if (i < jul->max_iter && jul->cmode != NORM)
	{
		tmp = log(log((z.a * z.a + z.b * z.b) / 2.)) / log(2);
		i = i + 2 - tmp;
	}
	plot_point(p, i, jul, ml);
}
