/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   utils.c                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/12 21:15:13 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 16:37:55 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"
#include <math.h>

unsigned int	lempx_rgb(unsigned int r, unsigned int g, unsigned int b)
{
	if ((r > 255) | (g > 255) | (b > 255))
		return (0);
	return ((r << 16) | (g << 8) | b);
}

unsigned int	color_interpolation(t_color c1, t_color c2, double p)
{
	t_color				new_col;

	if (c1.color == c2.color)
		return (c1.color);
	new_col.rgb[3] = 0;
	new_col.rgb[2] = (char)((1 - p) * c1.rgb[2] + p * c2.rgb[2]);
	new_col.rgb[1] = (char)((1 - p) * c1.rgb[1] + p * c2.rgb[1]);
	new_col.rgb[0] = (char)((1 - p) * c1.rgb[0] + p * c2.rgb[0]);
	return (new_col.color);
}

void			plot_point(t_point p, double iter,
		t_param *jul, t_mlx_struct *ml)
{
	int		po;
	double	tmp;
	double	bin;
	t_color	col1;
	t_color	col2;

	tmp = (int)((iter / (double)jul->max_iter) * 255);
	po = p.x + (SCREEN_Y - p.y - 1) * SCREEN_X;
	if (iter == jul->max_iter)
		ml->img_data[po] = mlx_get_color_value(ml->mlx_ptr, lempx_rgb(0, 0, 0));
	else if (jul->cmode == SMOOTH || jul->cmode == INV)
	{
		col1 = jul->palette[(int)floor(iter) % (jul->pal_size)];
		col2 = jul->palette[(int)floor(iter + 1) % jul->pal_size];
		tmp = modf(iter, &bin);
		if (jul->cmode == SMOOTH)
			ml->img_data[po] = color_interpolation(col1, col2, tmp);
		else
			ml->img_data[po] = color_interpolation(col2, col1, tmp);
	}
	else
	{
		col1 = jul->palette[(int)floor(iter) % jul->pal_size];
		ml->img_data[po] = col1.color;
	}
}

t_complex		mapset(t_point p, t_param *jul)
{
	double	sizey;
	double	sizex;
	double	wherex;
	double	wherey;

	sizex = 5 / jul->zoom;
	sizey = sizex * RATIO;
	wherex = p.x / (double)(SCREEN_X - 1);
	wherey = (1 - p.y / (double)(SCREEN_Y - 1));
	return (init_complex(-2.5 + jul->beg_space.a * 5 + wherex * sizex,
				-2.5 * RATIO + jul->beg_space.b * 5 * RATIO + wherey * sizey));
}

int				ft_memdel_return(void **t)
{
	ft_memdel(t);
	return (0);
}
