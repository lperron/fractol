/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lsys_input.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/12 22:55:33 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 19:16:38 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"
#include "lsys.h"
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <math.h>

double	ft_atod(char *str)
{
	double	ret;
	double	tmp;
	int		i;

	i = 0;
	ret = ft_atoi(str);
	while (str[i] && str[i] <= '9' && str[i] >= '0')
		i++;
	if (str[i] == '.' && str[i + 1] <= '9' && str[i + 1] >= '0')
	{
		tmp = ft_atoi(&str[i + 1]);
		while (fabs(tmp) > 0.99999999999)
			tmp /= 10.;
		ret += ret >= 0 ? tmp : -1 * tmp;
	}
	return (ret);
}

int		get_param(t_lsys *sys, int fd)
{
	char	*str;

	while (get_next_line(fd, &str))
	{
		if (ft_strncmp(str, "angle:", 6) == 0)
			sys->angle = ft_atod(&str[6]);
		else if (ft_strncmp(str, "length:", 7) == 0)
			sys->length = ft_atod(&str[7]);
		else if (ft_strncmp(str, "startx:", 7) == 0)
			sys->strt_point.a = ft_atod(&str[7]) * SCREEN_X;
		else if (ft_strncmp(str, "starty:", 7) == 0)
			sys->strt_point.b = (1 - ft_atod(&str[7])) * SCREEN_Y;
		else if (ft_strncmp(str, "scale_factor:", 13) == 0)
			sys->scale_factor = ft_atod(&str[13]);
		else if (ft_strncmp(str, "iter:", 5) == 0)
			sys->iter = ft_atoi(&str[5]);
		else if (ft_strncmp(str, "start_angle:", 12) == 0)
			sys->strt_angle = ft_atod(&str[12]);
		else if (!(str[0] == 0 || str[0] == '#'))
			return (ft_memdel_return((void**)&str));
		ft_memdel((void**)&str);
	}
	return (1);
}

int		get_rules(t_lsys *sys, int fd)
{
	int	nextone;

	nextone = 0;
	sys->num = 0;
	while (get_next_line(fd, &sys->rules[sys->num]))
	{
		if (sys->rules[sys->num][0] == 0 || sys->rules[sys->num][0] == '#')
			ft_memdel((void**)&sys->rules[sys->num]);
		else if (!nextone && ft_strcmp(sys->rules[sys->num], "RULES :") == 0)
			nextone = 1;
		else if (nextone)
		{
			if (ft_strcmp(sys->rules[sys->num], "PARAMS :") == 0)
				return (sys->num > 0 && get_param(sys, fd));
			if (!check_rule(sys->rules[sys->num]) || !(++(sys->num)))
				return (0);
			if (sys->num == 100)
				return (0);
		}
		else
			return (0);
		if (sys->num == 0)
			ft_memdel((void**)&sys->rules[0]);
	}
	return (sys->num > 0 ? 1 : 0);
}

int		get_axiom(t_lsys *sys, int fd)
{
	int	nextistheone;

	nextistheone = 0;
	while (get_next_line(fd, &sys->axiom))
	{
		if (sys->axiom[0] == 0 || sys->axiom[0] == '#')
		{
			if (!sys->name && sys->axiom[0] == '#')
				sys->name = ft_strdup(&sys->axiom[1]);
		}
		else if (!nextistheone && ft_strcmp(sys->axiom, "AXIOM :") == 0)
			nextistheone = 1;
		else if (nextistheone)
			break ;
		else
		{
			ft_memdel((void**)&sys->axiom);
			return (0);
		}
		ft_memdel((void**)&sys->axiom);
	}
	return (sys->axiom ? check_axiom(sys->axiom) : 0);
}

t_lsys	*get_lsys(char *file)
{
	int		fd;
	t_lsys	*sys;

	if ((fd = open(file, O_RDONLY)) < 0 || read(fd, NULL, 0) < 0)
	{
		close(fd);
		ft_printf("Error : could not read %s\n", file);
		return (NULL);
	}
	if (!(sys = ft_memalloc(sizeof(t_lsys))))
	{
		close(fd);
		ft_putendl("Error : Not enough memory");
		free_sys(sys);
		return (NULL);
	}
	init_lsys(sys);
	if (!get_axiom(sys, fd) || !get_rules(sys, fd) || !other_check(sys))
	{
		close(fd);
		ft_putendl("Error : File not well formated");
		free_sys(sys);
		return (NULL);
	}
	return (sys);
}
