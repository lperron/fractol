/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lsys_main.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/14 19:48:42 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 14:35:44 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lsys.h"
#include <unistd.h>

void	paint_it_almost_black(int *img_data, int nb)
{
	t_color tmp;
	int		i;

	tmp.color = 0;
	tmp.rgb[0] = 30;
	tmp.rgb[1] = 30;
	tmp.rgb[2] = 30;
	i = -1;
	while (++i < nb)
		img_data[i] = tmp.color;
}

int		lsys_loop_hook(t_lstruct *ll)
{
	int	tmp;

	if (ll->arrow[0] && (ll->sys->change = 1))
		ll->sys->strt_point.b += 5;
	if (ll->arrow[1] && (ll->sys->change = 1))
		ll->sys->strt_point.b -= 5;
	if (ll->arrow[2] && (ll->sys->change = 1))
		ll->sys->strt_point.a += 5;
	if (ll->arrow[3] && (ll->sys->change = 1))
		ll->sys->strt_point.a -= 5;
	if (!ll->sys->change)
		usleep(10000);
	if (!ll->sys->change)
		return (0);
	usleep(20000);
	paint_it_almost_black(ll->ml->img_data, ll->ml->nb_pxl);
	lsys_interpreter(ll);
	tmp = ft_max(10 + (int)(ft_strlen(ll->sys->name) + 11) *
			X_C + 3, 10 + 20 * X_C + 3);
	lsys_print_frame(ll, tmp);
	mlx_put_image_to_window(ll->ml->mlx_ptr, ll->ml->win_ptr,
		ll->ml->img, 0, 0);
	lsys_print_info(ll);
	ll->sys->change = 0;
	return (0);
}

void	lsys_fractol(char **argv)
{
	t_lsys			*sys;
	t_lstruct		ll;
	t_mlx_struct	ml;

	sys = get_lsys(argv[2]);
	if (!sys)
		return ;
	sys->change = 1;
	init_mlx(SCREEN_X, SCREEN_Y, "fractol", &ml);
	ll.ml = &ml;
	ll.sys = sys;
	ll.arrow[0] = 0;
	ll.arrow[1] = 0;
	ll.arrow[2] = 0;
	ll.arrow[3] = 0;
	if (!lsys_recur(sys))
		return ;
	mlx_loop_hook(ll.ml->mlx_ptr, lsys_loop_hook, &ll);
	mlx_hook(ll.ml->win_ptr, KEYPRESS, KEYPRESSMASK, lsys_handle_key_pressed,
			&ll);
	mlx_hook(ll.ml->win_ptr, KEYRELEASE, KEYRELEASEMASK,
			lsys_handle_key_released, &ll);
	mlx_hook(ll.ml->win_ptr, DESTROYNOTIFY,
		STRUCTURENOTIFYMASK, lsys_destroy_hook, &ll);
	mlx_loop(ll.ml->mlx_ptr);
}
