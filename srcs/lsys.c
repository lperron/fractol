/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lsys.c                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/14 18:51:48 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 18:53:33 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include <stdlib.h>
#include "lsys.h"

void	free_sys(t_lsys *sys)
{
	int i;

	i = -1;
	while (++i < 100)
		free(sys->rules[i]);
	free(sys->str_iter);
	free(sys->axiom);
	free(sys->name);
	free(sys);
}

void	init_lsys(t_lsys *sys)
{
	int	i;

	i = -1;
	sys->axiom = NULL;
	while (++i < 100)
		sys->rules[i] = NULL;
	sys->num = 0;
	sys->str_iter = NULL;
	sys->angle = -90;
	sys->length = 50;
	sys->iter = 0;
	sys->name = NULL;
	sys->strt_point = init_complex(SCREEN_X / 2., SCREEN_Y / 2.);
	sys->scale_factor = 1.25;
	sys->iter = 5;
	sys->strt_angle = 90;
}
