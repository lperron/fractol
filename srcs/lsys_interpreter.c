/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lsys_interpreter.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/13 15:18:27 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 19:22:39 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "lsys.h"
#include <math.h>

void	move_turtle(t_lstruct *ll, double *angle_length, t_complex *p, char c)
{
	t_complex	new;
	t_point		tmp;
	t_point		tmp2;
	double		si;
	double		co;

	si = sin(angle_length[0] / 180. * (double)M_PI);
	co = cos(angle_length[0] / 180. * (double)M_PI);
	new = init_complex(p->a + angle_length[1]
			* co, p->b + angle_length[1] * si);
	if (c <= 'Z')
	{
		tmp = init_point(new.a, new.b);
		tmp2 = init_point(p->a, p->b);
		draw_line(&tmp2, &tmp, ll->ml);
	}
	*p = new;
}

void	next_command(t_lstruct *ll, char s, double *angle_length)
{
	if (s == '<')
		angle_length[1] /= ll->sys->scale_factor;
	else if (s == '>')
		angle_length[1] *= ll->sys->scale_factor;
	else if (s == '+')
		angle_length[0] += ll->sys->angle;
	else if (s == '-')
		angle_length[0] -= ll->sys->angle;
	else if (s == '|')
		angle_length[0] -= 180;
}

int		lsys_interpreter_rec(t_lstruct *ll, char *s, double *a_l, t_complex p)
{
	int		i;
	double	angle_length[2];

	i = -1;
	angle_length[0] = a_l[0];
	angle_length[1] = a_l[1];
	while (s[++i])
	{
		if (s[i] == ']')
			return (i + 1);
		else if (s[i] == '[')
			i += lsys_interpreter_rec(ll, &s[i + 1], angle_length, p);
		else if ((ft_tolower(s[i]) >= 'x' || ft_tolower(s[i]) <= 'Z')
				&& ft_isalpha(s[i]))
			;
		else if (ft_isalpha(s[i]))
			move_turtle(ll, angle_length, &p, s[i]);
		next_command(ll, s[i], angle_length);
		if (!s[i])
			break ;
	}
	return (i + 1);
}

void	lsys_interpreter(t_lstruct *ll)
{
	double angle_length[2];

	angle_length[0] = ll->sys->strt_angle;
	angle_length[1] = ll->sys->length;
	if (ll->sys->str_iter)
		lsys_interpreter_rec(ll, ll->sys->str_iter, angle_length,
				ll->sys->strt_point);
}
