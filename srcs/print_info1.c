/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_info1.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/12 21:44:25 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 17:34:41 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"

void		print_fractal(t_hook *data)
{
	if (data->fract->type == JULIA)
		mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
10, Y_OFF, lempx_rgb(255, 255, 255), "Fractal : Julia");
	else if (data->fract->type == MANDELBROT)
		mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
10, Y_OFF, lempx_rgb(255, 255, 255), "Fractal : Mandelbrot");
	else if (data->fract->type == TRICORN)
		mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
10, Y_OFF, lempx_rgb(255, 255, 255), "Fractal : Tricorn");
	else if (data->fract->type == BURNING)
		mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
10, Y_OFF, lempx_rgb(255, 255, 255), "Fractal : Burning Ship");
	else if (data->fract->type == LEMP1)
		mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
10, Y_OFF, lempx_rgb(255, 255, 255), "Fractal : Oogie Boogie");
	else if (data->fract->type == LEMP2)
		mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
10, Y_OFF, lempx_rgb(255, 255, 255), "Fractal : Orchis");
	else if (data->fract->type == JULIAB)
		mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
10, Y_OFF, lempx_rgb(255, 255, 255), "Fractal : Julia's Burning");
}

void		print_color_mode(t_hook *data)
{
	if (data->fract->param->cmode == SMOOTH)
		mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
		10, Y_OFF + 14, lempx_rgb(255, 255, 255), "Color : Smooth");
	if (data->fract->param->cmode == INV)
		mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
		10, Y_OFF + 14, lempx_rgb(255, 255, 255), "Color : Inverse");
	if (data->fract->param->cmode == NORM)
		mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
		10, Y_OFF + 14, lempx_rgb(255, 255, 255), "Color : Normal");
}

void		print_iter(t_hook *data)
{
	char *str;
	char *tmp;

	tmp = ft_itoa(data->fract->param->max_iter);
	if (!tmp)
		return ;
	str = ft_strjoin("Iterations : ", tmp);
	free(tmp);
	if (!str)
		return ;
	mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
		10, Y_OFF + 28, lempx_rgb(255, 255, 255), str);
	free(str);
}

void		print_radius(t_hook *data)
{
	char *str;
	char *tmp;

	tmp = ft_itoa(data->fract->param->radius);
	if (!tmp)
		return ;
	str = ft_strjoin("Radius : ", tmp);
	free(tmp);
	if (!str)
		return ;
	mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
		10, Y_OFF + 42, lempx_rgb(255, 255, 255), str);
	free(str);
}
