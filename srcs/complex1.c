/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   complex1.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/10 16:54:42 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 18:34:10 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "complex.h"

t_complex	init_complex(double a, double b)
{
	t_complex ret;

	ret.a = a;
	ret.b = b;
	return (ret);
}

t_complex	cadd(t_complex c1, t_complex c2)
{
	return (init_complex(c1.a + c2.a, c1.b + c2.b));
}

t_complex	csub(t_complex c1, t_complex c2)
{
	return (init_complex(c1.a - c2.a, c1.b - c2.b));
}

t_complex	cmult(t_complex c1, t_complex c2)
{
	return (init_complex(c1.a * c2.a - c1.b * c2.b,
				c1.a * c2.b + c2.a * c1.b));
}

t_complex	cdiv(t_complex c1, t_complex c2)
{
	double r;

	r = 1. / (c2.a * c2.a + c2.b * c2.b);
	return (init_complex(r * (c1.a * c2.a + c1.b * c2.b),
				r * (c1.b * c2.a - c1.a * c2.b)));
}
