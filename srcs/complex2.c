/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   complex2.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/10 17:10:35 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 18:36:50 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "complex.h"
#include <math.h>

t_complex		cconj(t_complex c)
{
	return (init_complex(c.a, -1 * c.b));
}

/*
** here we're gonna use DeMoivre's theorem
** but first of all we need to have our complex number in polar form
** a + ib -> r(cos(theta) + isin(theta))
**then DeMoivre :
** c ^ n = (r ^ n) *(cos(n * theta) + isin(n * theta))
** (but if n = 2 we simply return the cmult c * c)
*/

t_complex		cpowint(t_complex c, int n)
{
	t_complex res;

	res.a = c.a;
	res.b = c.b;
	while (--n)
		res = cmult(res, c);
	return (res);
}

t_complex		cpower(t_complex c, double n)
{
	double	r;
	double	th;
	double	i_n;

	if (modf(n, &i_n) == 0)
		return (cpowint(c, i_n));
	r = cmod(c);
	th = cargu(c);
	return (c_from_polar(pow(r, n), n * th));
}

t_complex		croot(t_complex c, double root)
{
	return (cpower(c, 1. / root));
}

void			print_complex(t_complex c)
{
	ft_printf("%f + i * %f\n", c.a, c.b);
}
