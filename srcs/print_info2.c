/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_info2.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/12 21:49:39 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 14:36:25 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "fractol.h"
#include <math.h>

char		*ft_dtoa_cheat(double d, int signi)
{
	char	*tmp;
	char	*tmp2;
	char	*str;
	double	int_p;
	double	deci_p;

	tmp = NULL;
	tmp2 = NULL;
	deci_p = modf(d, &int_p);
	tmp = ft_itoa(fabs(int_p));
	tmp2 = ft_itoa((fabs(deci_p) * pow(10, signi)));
	if ((!tmp || !tmp2) && !ft_super_free(2, tmp, tmp2))
		return (NULL);
	if (d >= 0)
		str = ft_strjoin_mult(3, tmp, ".", tmp2);
	else
		str = ft_strjoin_mult(4, "- ", tmp, ".", tmp2);
	ft_super_free(2, tmp, tmp2);
	return (str);
}

void		print_power(t_hook *data)
{
	char	*str;
	char	*tmp;
	char	*tmp2;

	tmp = ft_itoa(data->fract->param->power.x);
	tmp2 = ft_itoa(data->fract->param->power.y);
	if ((!tmp || !tmp2) && ft_super_free(2, tmp, tmp2))
		return ;
	str = ft_strjoin_mult(4, "Power : ", tmp, ".", tmp2);
	free(tmp);
	free(tmp2);
	if (!str)
		return ;
	mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
		10, Y_OFF + 56, lempx_rgb(255, 255, 255), str);
	free(str);
}

void		print_c(t_hook *data)
{
	char *str;
	char *tmp1;
	char *tmp2;

	tmp1 = NULL;
	tmp2 = NULL;
	tmp1 = ft_dtoa_cheat(data->fract->param->c.a, 4);
	tmp2 = ft_dtoa_cheat(data->fract->param->c.b, 4);
	if ((!tmp1 || !tmp2) && !ft_super_free(2, tmp1, tmp2))
		return ;
	if (data->fract->param->c.b >= 0)
		str = ft_strjoin_mult(5, "c = ", tmp1, " + ", tmp2, "i");
	else
		str = ft_strjoin_mult(5, "c = ", tmp1, " ", tmp2, "i");
	ft_super_free(2, tmp1, tmp2);
	mlx_string_put(data->ml->mlx_ptr, data->ml->win_ptr,
		10, Y_OFF + 70, lempx_rgb(255, 255, 255), str);
	free(str);
}

void		print_info(t_hook *data)
{
	print_fractal(data);
	print_color_mode(data);
	print_iter(data);
	print_radius(data);
	print_power(data);
	if (data->fract->type == JULIA || data->fract->type == JULIAB)
		print_c(data);
}
