/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   fractol.h                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/11 09:15:10 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 17:38:52 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H
# include "libft.h"
# include "t_point.h"
# include "mlx.h"
# include "complex.h"
# include "lempxdef.h"
# include <stdlib.h>
# define NB_THREAD 8
# define SCREEN_X 1920
# define SCREEN_Y 1080
# define RATIO 0.5625
# define INVLOG2 3.32192809489
# define SMOOTH 0
# define INV 1
# define NORM 2
# define ZOOM_FACTOR1 1.1
# define ZOOM_FACTOR2 1.01

# define JULIA 0
# define MANDELBROT 1
# define TRICORN 2
# define BURNING 3
# define LEMP1 4
# define LEMP2 5
# define JULIAB 6

# ifdef __APPLE__
#  define Z_CHAR 13
#  define S_CHAR 1
#  define Q_CHAR 0
#  define D_CHAR 2
#  define O_CHAR 31
#  define I_CHAR 34
#  define P_CHAR 35
#  define C_CHAR 8
#  define M_CHAR 46
#  define R_CHAR 15
#  define F_CHAR 3
#  define ESC_CHAR 53
#  define SCROLL_DOWN 5
#  define SCROLL_UP 4

#  define SPACE_CHAR 49
#  define CTRL_CHAR 256
#  define SHIFT_CHAR 257
#  define UP_CHAR 126
#  define DOWN_CHAR 125
#  define LEFT_CHAR 123
#  define RIGHT_CHAR 124
#  define E_CHAR 14
#  define PLUS_CHAR 69
#  define MINUS_CHAR 78
#  define Y_OFF 0
#  define Y_MORE 20
#  define X_MORE 60
#  define JULIA_OFF 35
#  define X_C 10
#  define Y_C 10

# else
#  define Z_CHAR 'z'
#  define S_CHAR 's'
#  define Q_CHAR 'q'
#  define D_CHAR 'd'
#  define O_CHAR 'o'
#  define I_CHAR 'i'
#  define P_CHAR 'p'
#  define C_CHAR 'c'
#  define M_CHAR 'm'
#  define R_CHAR 'r'
#  define F_CHAR 'f'
#  define L_CHAR 'l'
#  define K_CHAR 'k'
#  define E_CHAR 'e'
#  define R_CHAR 'r'
#  define SPACE_CHAR 32
#  define MINUS_CHAR 'm'
#  define PLUS_CHAR 'p'
#  define ESC_CHAR 65307
#  define CTRL_CHAR 65507
#  define UP_CHAR 65362
#  define DOWN_CHAR 65364
#  define LEFT_CHAR 65361
#  define RIGHT_CHAR 65363
#  define SHIFT_CHAR 65505
#  define SCROLL_UP 5
#  define SCROLL_DOWN 4
#  define Y_OFF 20
#  define X_MORE 0
#  define Y_MORE 0
#  define Y_C 10
#  define X_C 6
#  define JULIA_OFF 0
# endif

union							u_color
{
	unsigned char				rgb[4];
	unsigned int				color;
};

typedef	union u_color			t_color;

struct							s_param
{
	t_point						power;
	t_complex					c;
	int							max_iter;
	double						zoom;
	t_complex					beg_space;
	t_color						palette[250];
	int							i_pal;
	int							pal_size;
	int							cmode;
	int							radius;
};

typedef struct s_param			t_param;

struct							s_mlx_struct
{
	void						*mlx_ptr;
	void						*win_ptr;
	void						*img;
	int							*img_data;
	int							bits_per_pixel;
	int							size_line;
	int							endian;
	int							nb_pxl;
};
typedef struct s_mlx_struct		t_mlx_struct;

struct							s_fractalizer
{
	int							type;
	t_param						*param;
	int							change;
	void						(*fract_func)(t_point p, t_param *jul,
								t_mlx_struct *ml);
};

typedef struct s_fractalizer	t_fractalizer;

struct							s_thread_data
{
	t_point						strt;
	t_fractalizer				*fract;
	t_mlx_struct				*ml;
};

typedef struct s_thread_data	t_thread_data;

struct							s_hook
{
	pthread_t					*thr;
	t_fractalizer				*fract;
	t_mlx_struct				*ml;
	char						control;
	char						shift;
};

typedef struct s_hook			t_hook;

/*
** thread.c
*/
void							fractalize(t_fractalizer *fract,
									t_mlx_struct *ml, pthread_t *thr);

/*
** julia.c
*/
void							julia_point(t_point p, t_param *jul,
									t_mlx_struct *ml);
void							juliab_point(t_point p, t_param *jul,
									t_mlx_struct *ml);

/*
** mandelbrot.c
*/
void							mandel_point(t_point p, t_param *param,
									t_mlx_struct *ml);
void							tricorn_point(t_point p, t_param *param,
									t_mlx_struct *ml);
void							burning_point(t_point p, t_param *param,
									t_mlx_struct *ml);
void							lemp1_point(t_point p, t_param *param,
									t_mlx_struct *ml);
void							lemp2_point(t_point p, t_param *param,
									t_mlx_struct *ml);

/*
** utils.c
*/
t_complex						mapset(t_point p, t_param *jul);
unsigned int					lempx_rgb(unsigned int r, unsigned int g,
										unsigned int b);
void							plot_point(t_point p, double iter, t_param
									*param, t_mlx_struct *ml);
int								ft_memdel_return(void **t);

/*
** mouse_hook.c
*/
int								mouse_button_hook(int button, int x, int y,
									t_hook *data);
int								mouse_hook(int x, int y, t_hook *data);

/*
** print_info1.c
*/
void							print_fractal(t_hook *data);
void							print_color_mode(t_hook *data);
void							print_iter(t_hook *data);
void							print_radius(t_hook *data);

/*
** print_info2.c
*/
char							*ft_dtoa_cheat(double d, int signi);
void							print_power(t_hook *data);
void							print_c(t_hook *data);
void							print_info(t_hook *data);

/*
** set_fractal.c
*/
void							set_fractal(t_fractalizer *fract, int type);
void							next_fractal(t_fractalizer *fract);

/*
** key_hook.c
*/
int								destroy_hook(t_hook *data);
int								handle_key_pressed(int key, t_hook *data);
int								handle_key_released(int key, t_hook *data);

/*
** mlx_things.c
*/
int								loop_hook(t_hook *data);
int								init_img(t_mlx_struct *tt);
int								init_mlx(int x, int y, char *name,
									t_mlx_struct *tt);

/*
** palette_switch.c
*/
void							switch_pal(t_hook *data);
/*
** palette.c
*/
void							init_palette(t_param *sys);
void							init_palette1(t_param *sys);
void							init_palette2(t_param *sys);
void							init_palette3(t_param *sys);
void							init_palette4(t_param *sys);
#endif
