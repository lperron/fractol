/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   lsys.h                                           .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/13 10:37:22 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/15 13:51:46 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef LSYS_H
# define LSYS_H
# include "fractol.h"

struct						s_lsys
{
	char					*axiom;
	char					*rules[100];
	int						num;
	char					*str_iter;
	double					angle;
	double					scale_factor;
	double					length;
	int						iter;
	char					*name;
	char					change;
	t_complex				strt_point;
	double					strt_angle;
	int						str_size;
};

typedef struct s_lsys		t_lsys;

struct						s_lstruct
{
	t_lsys					*sys;
	t_mlx_struct			*ml;
	char					arrow[4];
};

typedef struct s_lstruct	t_lstruct;
/*
** lsys_recur.c
*/
int							get_inser_size(char *str, t_lsys *sys);
int							lsys_recur(t_lsys *sys);
/*
** lsys_input.c
*/
t_lsys						*get_lsys(char *file);
/*
** bresenham.c
*/
void						draw_line(t_point *v, t_point *v2,
								t_mlx_struct *ml);
/*
** lsys_interpreter.c
*/
void						lsys_interpreter(t_lstruct *s);

/*
**  lsys_key_hook.c
*/
int							lsys_destroy_hook(t_lstruct *data);
int							lsys_handle_key_pressed(int key, t_lstruct *data);
int							lsys_handle_key_released(int key, t_lstruct *data);

/*
** lsys_print_info.c
*/
void						lsys_print_frame(t_lstruct *ll, int n);
void						lsys_print_info(t_lstruct *data);

/*
** lsys_check.c
*/
int							check_axiom(char *str);
int							check_rule(char *str);
int							other_check(t_lsys *sys);

/*
** lsys.c
*/
void						free_sys(t_lsys *sys);
void						init_lsys(t_lsys *sys);
/*
** lsys_main.c
*/
int							lsys_loop_hook(t_lstruct *ll);
void						lsys_fractol(char **argv);
#endif
