/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   complex.h                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: lperron <lperron@student.le-101.f>         +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/08/10 16:40:49 by lperron      #+#   ##    ##    #+#       */
/*   Updated: 2019/08/14 23:39:49 by lperron     ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef COMPLEX_H
# define COMPLEX_H

# include "libft.h"

struct						s_complex
{
	double					a;
	double					b;
};

typedef struct s_complex	t_complex;

t_complex					init_complex(double a, double b);
t_complex					cadd(t_complex c1, t_complex c2);
t_complex					csub(t_complex c1, t_complex c2);
t_complex					cmult(t_complex	c1, t_complex c2);
t_complex					cdiv(t_complex	c1, t_complex c2);

/*
** complex2.c
*/

t_complex					cconj(t_complex c);
t_complex					cpowint(t_complex c, int n);
t_complex					cpower(t_complex c, double p);
t_complex					croot(t_complex c, double root);
void						print_complex(t_complex c);
/*
** complex3.c
*/

double						cmod(t_complex c);
double						cargu(t_complex c);
t_complex					c_from_polar(double r, double th);
t_complex					cpowerfast(t_complex c,
						int ni, int nd);
#endif
